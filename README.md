# Breakout project in JavaFX 
The game is pretty simple, just excute compiled files (after you import the project into Eclipse). The most cool feature is the level-EDITOR

### Basic Instructions
1) SPACE to pause
2) F1 restart
3) F2 open level editor
4) F5 choose level from the folder "Livelli"

### Preview
www.youtube.com/watch?v=eFZDLtMpSDs


### Credits
@rageSpin <--> Stefano Giannini