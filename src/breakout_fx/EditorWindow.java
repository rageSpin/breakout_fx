/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package breakout_fx;

import java.io.File;
import java.util.ArrayList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import static javafx.scene.text.FontWeight.BOLD;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author stefano giannini
 */

public class EditorWindow extends Stage{ 
	
    private final Group group;
    private final Pane pane;
    private final BorderPane borderPane;
    private final Scene scene;
    private final BrickGrid brickGrid;
    private ArrayList<Rectangle> skeleton;
    private Brick tempB; 
    private int index, type;
    private char size;
    
    /**
     * Editor Window Stage 
     * 
     * @param screenWidth
     * @param screenHeight
     * @param maxRow
     * @param maxCol
    */
    
    public EditorWindow(double screenWidth, double screenHeight, int maxRow, int maxCol){
        //initialization
        group = new Group();
        pane = new Pane(group);
        borderPane = new BorderPane();
        brickGrid = new BrickGrid(maxRow, maxCol);
        pane.setPrefSize(screenWidth, screenHeight);
        pane.setStyle("-fx-background-color: blanchedalmond;");
        scene = new Scene(borderPane, screenWidth, screenHeight);
        skeleton = new ArrayList<Rectangle>(); 
        
        borderPane.setCenter(pane);
        borderPane.setBottom(createLegendBox());
        // creating and drawing skeleton
        setDefault();
        
        // setting and drawing first temporary brick
        index = maxCol*(maxRow-1)/2 -1;
        size = 'm';
        type = 1;
       
        drawTempBrick();
        handleKeyPress();
        
        setTitle("Editor Breakout");
        setResizable(false);
        setScene(scene);
        show();
    }
    
    private void handleKeyPress(){
        scene.setOnKeyPressed((KeyEvent e) -> {
            switch(e.getCode()){
                case LEFT:
                    if(index > brickGrid.getMaxRow() - 1){
                        index -= brickGrid.getMaxRow();
                    }
                    break;
                case RIGHT:
                    if(index < brickGrid.getMaxCol() * brickGrid.getMaxRow() - brickGrid.getMaxRow() ){
                        index += brickGrid.getMaxRow();
                    }
                    break;
                case UP:
                    if(index > 0 && (index) % brickGrid.getMaxRow() != 0){
                        index--;
                    }
                    break;
                case DOWN:
                    if(index < brickGrid.getMaxCol() * brickGrid.getMaxRow() &&
                            (index + 1) % brickGrid.getMaxRow() != 0){
                        index++;
                    }
                     break;
                case ENTER:
                    // adding to group root a full brick
                    if(checkBrick() < 0){
                        Brick bi = new Brick(tempB.getXCenter(), tempB.getYCenter(), tempB.getType(), tempB.getSize());
                        bi.drawSize();
                        bi.drawType();
                        bi.setOpacity(1);
                        group.getChildren().remove(tempB); 
                        brickGrid.getBricks().add(bi);
                        group.getChildren().add(bi);   
                    }
                    break;
                case F1:
                    //clear brick
                    setDefault();
                    break;
                case D:
                    // delete one brick
                    if(checkBrick() >= 0){
                        group.getChildren().remove(brickGrid.getBrick(checkBrick()));
                        brickGrid.getBricks().remove(checkBrick());
                    }
                    break;
                case G:
                    if(tempB.getSize() == 'm'){
                        size = 's';
                    }
                    else{
                        size = 'm';
                    }
                    break;
                case T:
                    if(tempB.getType() < 4){
                        type++;
                    }
                    else{
                        type = 1;
                    }
                    System.out.println("Aumento tipo");
                    break;
                case S:
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Salva livello");
                    fileChooser.getExtensionFilters().addAll(
                                new FileChooser.ExtensionFilter("Text Files", "*.txt"));
                    fileChooser.setInitialDirectory(new File("Livelli/"));
                    fileChooser.setInitialFileName("livelloX.txt");
                    File fileToSave = fileChooser.showSaveDialog(this);
                    if(fileToSave != null) {
                    	brickGrid.saveLevel(fileToSave);
                    }
                    break;
                case ESCAPE:
                    close();
                default:
                    return;
            }
            drawTempBrick();
            System.out.println(index);
        });
    }
    
    //return a short to specify also the brick number to delete 
    private short checkBrick(){
        if(!brickGrid.getBricks().isEmpty()){
            for(short i = 0; i < brickGrid.getBricks().size(); i++){
                if(brickGrid.getBrick(i).contains(tempB.getXCenter(), tempB.getYCenter())){
                    return i;
                }   
            }
            return -1;
        }
        else{
            return -1;
        }
    }
    
    private void drawTempBrick(){
        group.getChildren().remove(tempB);
        
        tempB = new Brick(skeleton.get(index).getX(), skeleton.get(index).getY(), type, size);    
        tempB.drawType();
        tempB.drawSize();
        tempB.setOpacity(0.4);
        if(checkBrick() < 0){
            group.getChildren().add(tempB);
        }
    }
    
    private void setDefault(){
        brickGrid.getBricks().clear();
        group.getChildren().clear();
        
        skeleton = brickGrid.createSkeleton();
        skeleton.forEach((Rectangle r) -> {
            group.getChildren().add(r);
        });
    }
    
    private HBox createLegendBox(){
        HBox hbox = new HBox(20);
        Text instructionsTitle = new Text("Come usarlo? \n\n" 
                                + "Muoversi: \n"
                                + "Pulire la griglia: \n"
                                + "Inserimento mattoncino: \n"
                                + "Eliminazione mattoncino: \n"
                                + "Salvataggio Livello \n"
                                + "Cambiare tipo di mattoncino: \n"
                                + "Cambiare dimensioni del mattoncino: \n"
                                + "Uscire: ");
        new Font(13);
		instructionsTitle.setFont(Font.font("Serif", BOLD , 13));
        Text instructions = new Text("\n\n" +
                                "Freccie direzionali \n"
                                + "F1 \n"
                                + "INVIO \n"
                                + "Tasto D \n"
                                + "Tasto S \n"
                                + "Tasto T \n"
                                + "Tasto G \n"
                                + "ESC ");
        instructions.setFont(new Font(13));
        hbox.getChildren().addAll(instructionsTitle, instructions);
        hbox.setMinWidth(scene.getWidth()/2 - 50);
        hbox.setTranslateX((scene.getWidth() - hbox.getMinWidth())/2);
        hbox.setPadding(new Insets(10));
        
        return hbox;
    }
}
