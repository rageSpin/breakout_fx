/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package breakout_fx;

import javafx.scene.image.ImageView;

/**
 *
 * @author giannini stefano
 * dX and dY goes from -1 to 1;
 */

public abstract class Sprite extends ImageView{ 
    private int speed;
    private double dX, dY;
    
    
    public void setSpeed(int speed){
        this.speed = speed;
    }
    
    public int getSpeed(){
        return speed;
    }
    
    public void setDX(double dX){
        this.dX = dX;
    }
    
    public double getDX(){
        return dX;
    }
    
    public void setDY(double dY){
        this.dY = dY;
    }
    
    public double getDY(){
        return dY;
    }
}

