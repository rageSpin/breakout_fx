/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package breakout_fx;

import javafx.scene.image.Image;

/**
 *
 * @author stefano giannini
 */

public class Ball extends Sprite{
    Image ballImage = new Image("resources/ball.png");
    
  
	/**
	 * Ball extends sprite, in the future I have to set a circular clip for the image
	 * so the game dynamic is more realistic
	 */
    
    public Ball(){
        setImage(ballImage);
        super.setSpeed(3);
        
    }
    
    public void invertDX() {
    	setDX(getDX() * -1);
    }
    
    public void invertDY() {
    	setDY(getDY() * -1);
    }
    
    public void increaseDX() {
    	if(getDX() < 1) {
    		setDX(getDX() + 0.05);
    	}
    }
    
    public void decreaseDX() {
    	if(getDX() > -1) {
    		setDX(getDX() - 0.05);
    	}
    }
   
}
