/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package breakout_fx;

import javafx.scene.image.Image;

/**
 *
 * @author stefano giannini
 */

public class Pad extends Sprite {
    private final Image padImage = new Image("resources/pad.png");
    
    //constructor
	/**
	 * 
	 */
    public Pad(){
        setImage(padImage);
    }
    
    /*@Override
    public void roundShape(Group root){
         Rectangle clip = new Rectangle(
                getFitWidth(), getFitHeight()
            );
            clip.setArcWidth(10);
            clip.setArcHeight(5);
            setClip(clip);
    }*/
   
}
