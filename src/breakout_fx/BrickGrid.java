/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package breakout_fx;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author oki
 */
public class BrickGrid {
    private ArrayList<Brick> bricks;
    private final int space, xMargin, yMargin, maxRow, maxCol, skelX, skelY;
    
    
    //inserire costruttore con importazione file di testo
    public BrickGrid(int row, int col){
        xMargin = 15;
        yMargin = 25;
        maxRow = row;
        maxCol = col;
        space = 10;
        skelX = 6;
        skelY = 6;
        bricks = new ArrayList<>();
    }

    public void loadLevel(File level){
        try{
            //if doesn't exist file -> make new standard file
        
            BufferedReader br2 = new BufferedReader(new FileReader(level));
            String readLine = "";
            bricks.clear();
            while ((readLine = br2.readLine()) != null) {
                 if(readLine.split(",").length > 1){
                    String[] st = readLine.split(","); 
                    String xPos = st[0].trim();
                    String yPos = st[1].trim();
                    String types = st[2].trim();
                    String sizes = st[3].trim();
                    
                    Brick b = new Brick(Double.parseDouble(xPos), 
                            Double.parseDouble(yPos), 
                            Integer.parseInt(types), 
                            sizes.charAt(0));
                    bricks.add(b);
                 }
            }
            br2.close();
        } catch(IOException e) {
			e.printStackTrace();
			System.err.println("The file was not found");
        }
    }
   
    public void saveLevel(File save) {
        try{
            save.getParentFile().mkdirs();
            save.createNewFile();
            
            BufferedWriter bWriter = new BufferedWriter(new FileWriter(save));
            String inputText;
            for(int i = 0; i < bricks.size() ; i++){
                inputText = Double.toString(bricks.get(i).getXCenter()) +", "
                            + Double.toString(bricks.get(i).getYCenter()) +", "
                            + Integer.toString(bricks.get(i).getType()) +", "
                            + Character.toString(bricks.get(i).getSize())+"\n";
                bWriter.write(inputText);
            }
            bWriter.close();
        
        }catch (IOException e) {
			e.printStackTrace();
        }
    
    }
    
    //creation of skeleton inside this method
    public ArrayList<Rectangle> createSkeleton(){
        Brick tempB = new Brick(0, 0, 1, 'm');
        tempB.drawSize();
        ArrayList<Rectangle> skeleton = new ArrayList<Rectangle>();
        try{
            for(int i = 0; i < maxCol; i++){
                for(int j = 0; j < maxRow; j++){
                    Rectangle r = new Rectangle(skelX, skelY, Color.DARKSALMON);
                    r.setX(xMargin + (tempB.getWidth() -skelX)/2 + i*(tempB.getWidth() + space));
                    r.setY(yMargin + (tempB.getHeight() -skelY)/2 + j*(tempB.getHeight() + space));
                    skeleton.add(r);                   
                }
            }
       }catch(NullPointerException e){
           System.out.println("array list inizializzato male");
       }
       return skeleton;
    }
    
    public void drawBricks(Group group){
       
        bricks.forEach((Brick b) -> {
            b.drawSize();
            b.drawType();
            group.getChildren().add(b);
        });
       
    }
    
    /** I use iterator to avoid exception 
     * ConcurrentModification
     * when some brick is eliminate
     * @param group
     * @param ball
     * @return return true if someone win (arraylist empty)
     */
    
    public boolean checkBricks(Group group, Ball ball){
        if(!bricks.isEmpty()){
            Iterator<Brick> it; 
            it = bricks.iterator();
            while(it.hasNext()) {
                Brick b = it.next();
                final Bounds bounds;
                bounds = b.getBoundsInLocal();
                    
                if(ball.intersects(bounds)){
                    boolean  brickBottom = ball.intersects(b.getX(), b.getY() + b.getHeight(), b.getWidth(), 0);
                    boolean  brickTop =  ball.intersects(b.getX(), b.getY(), b.getWidth(), 0);
                    boolean  brickLeft = ball.intersects(b.getX(), b.getY() , 0, b.getHeight());
                    boolean  brickRight = ball.intersects(b.getX() + b.getWidth() , b.getY(), 0, b.getHeight());
                    if(brickTop && ball.getDY() > 0) {
                        ball.invertDY();
                    }
                    if(brickLeft && ball.getDX() > 0) {
                        ball.invertDX();
                    }
                    if(brickRight && ball.getDX() < 0) {
                        ball.invertDX();
                    }
                    if(brickBottom && ball.getDY() < 0) {
                        ball.invertDY();
                    }
                    b.setType(b.getType() - 1);
                    System.out.println(ball.getDX() + ", " + ball.getDY());
                    if(b.getType() < 1){
                        it.remove();
                        group.getChildren().remove(b);
                    }
                }
            }
            return false;
        }
        else{
            return true;
        }
        
    }
    
    public Brick getBrick(int i){
        return bricks.get(i);
    }
   
    
    public ArrayList<Brick> getBricks(){
        return bricks;
    }
    
    public int getMaxRow(){
        return maxRow;
    }
    
    public int getMaxCol(){
        return maxCol;
    }
}
