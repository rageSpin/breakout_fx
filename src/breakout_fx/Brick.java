/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package breakout_fx;

import javafx.scene.paint.*;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author stefano giannini
 */

public class Brick extends Rectangle { //forse da togliere sprite

    private int type;
    private char size;
    private final double xCenter;
    private final double yCenter;
    private final int skelX, skelY, widthM, widthS, heightM, heightS;
    
    public Brick(double xCenter, double yCenter, int type, char size){
        this.type = type;
        this.size = size;
        this.xCenter = xCenter;
        this.yCenter = yCenter;
        skelX = skelY = 6;
        widthM = 60;
        widthS = heightM = heightS = 40;
    }
    
    public void drawType(){
        switch(type){
            case 1:
                Stop[] stop1 = new Stop[] { new Stop(0, Color.BROWN), new Stop(1, Color.BURLYWOOD)};
                LinearGradient lg1 = new LinearGradient(0, 0, 1, 0.3, true, CycleMethod.NO_CYCLE, stop1);
                setFill(lg1);
                setOpacity(1);
                break;
            case 2:
                Stop[] stop2 = new Stop[] { new Stop(0, Color.BLUE), new Stop(1, Color.LIGHTSKYBLUE)};
                LinearGradient lg2 = new LinearGradient(0.1, 0, 0.4, 0.9, true, CycleMethod.NO_CYCLE, stop2);
                setFill(lg2);
                break;
            case 3:
                Stop[] stop3 = new Stop[] { new Stop(0, Color.GREEN), new Stop(1, Color.DARKMAGENTA)};
                LinearGradient lg3 = new LinearGradient(0.12, 0.3, 0.8, 1, true, CycleMethod.NO_CYCLE, stop3);
                setFill(lg3);
                break;
            case 4:
                Stop[] stop4 = new Stop[] { new Stop(0, Color.GRAY), new Stop(1, Color.DARKSLATEGREY)};
                LinearGradient lg4 = new LinearGradient(0.1, 0.2, 0.9, 0.4, true, CycleMethod.NO_CYCLE, stop4);
                setFill(lg4);
                break;
            default:
                System.out.println("Mattoncino in posizione " + Double.toString(getX()) + ", " + Double.toString(getY()) + " eliminato");
      }
    }
    
    public int getType(){
        return type;
    }
    
    public void setType(int type){
        this.type = type;
        drawType(); //meglio dividere questi due metodi
    }
   
    public void drawSize(){
        if(size == 'm'){
            setWidth(widthM);
            setHeight(heightM);
      } else if (size == 's'){
            setWidth(widthS);
            setHeight(heightS);
      }
      setX(xCenter - (getWidth() - skelX)/2);
      setY(yCenter - (getHeight() - skelY)/2);
    }
    
    public char getSize(){
        return size;
    }
    
    public void setSize(char size){
        this.size = size;
        drawSize(); //meglio dividere questi due metodi
    }
    
    public double getXCenter(){
        return xCenter;
    }
    
    public double getYCenter(){
        return yCenter;
    }
    
    public int getSkelX(){
        return skelX;
    }
    
    public int getSkelY(){
        return skelY;
    }
    
    public int getMaxWidth(){
        return widthM;
    }
    
    public int getMaxHeight(){
        return heightM;
    }
    
}
