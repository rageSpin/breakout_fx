/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package breakout_fx;

import java.io.File;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import static javafx.scene.text.FontWeight.BOLD;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author stefano giannini
 */

public class Breakout_FX extends Application {
    private final double  SCREENWIDTH = 1000; //lasciare così, se cambia bisogna cambiare maxRow/maxCol
    private final double SCREENHEIGHT = 600;
    private Group group = new Group();
    private final Pane pane= new Pane(group);
    private boolean goLeft = false;
    private boolean goRight = false;
    private boolean pause;
    private final Pad pad = new Pad();
    private final Ball ball = new Ball();
    private Scene scene;
    private final int maxRow = 8;
    private final int maxCol= 14;
    private File fileLevel = new File("Livelli/livelloTCF.txt");
    
    private final Menu menuFile = new Menu("File");
    private final Menu menuOption = new Menu("Opzioni");
    private final Menu menuInfo = new Menu("?");
     
    private AnimationTimer aTimer;
    
    private final BrickGrid brickGrid = new BrickGrid(maxRow, maxCol);
    private final BorderPane borderPane = new BorderPane();
    private Stage primaryStage;
    
	// default layout
    private void setDefault() {
       group.getChildren().clear();
       pause = false;
       
       //adding pad
       group.getChildren().add(pad);
       pad.setFitWidth(200);
       pad.setFitHeight(25);
       pad.setDX(10);
       pad.setX(SCREENWIDTH/2 - pad.getFitWidth()/2 );
       pad.setY(SCREENHEIGHT - 3*pad.getFitHeight());
       //pad.setSmooth(true);
       
       //adding ball
       group.getChildren().add(ball);
       ball.setFitWidth(20);
       ball.setFitHeight(20);
       ball.setX(SCREENWIDTH/2 - ball.getFitWidth()/2 );
       ball.setY(pad.getY() - ball.getFitHeight());
       ball.setDY(1);
       ball.setDX(Math.random()*2 - 1);
       
       //drawing bricks
       brickGrid.drawBricks(group);
    }
    
    @Override
    public void start(Stage stage) {
        primaryStage = stage;
        
        //creating animation timer
        aTimer = new AnimationTimer(){
            @Override
            public void handle(long now){
                runBreakOut();
            }
        };
        
        // creating a menu
        createMenus();
        MenuBar menuBar = new MenuBar();
        menuBar.getMenus().addAll(menuFile, menuOption, menuInfo);
        menuBar.setUseSystemMenuBar(true);

        // setting dimension and background image
        pane.setPrefSize(SCREENWIDTH, SCREENHEIGHT);
        pane.setStyle("-fx-background-image: url('" + "https://media1.s-nbcnews.com/j/newscms/2017_05/1885986/ss-170131-misp-mn-01_72bfd4eb284b03e8248423ba19032856.nbcnews-ux-1024-900.jpg" + "'); " +
                "-fx-background-position: center center; " +
                "-fx-background-size: cover;"); 
        
        do{
        	System.out.println(group.localToScene(1, 1));
        	if(pane.localToScene(1, 1).getY() < 0) {
            	pane.setRotate(0);
            	
            	System.out.println(group.localToScene(1, 1));  
            }
        	if(pane.localToScene(1, 1).getX() < 0) {
        		pane.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
            }

        }while(pane.localToScene(1, 1).getX() < 0 || pane.localToScene(1, 1).getY() < 0);
        
        borderPane.setTop(menuBar);
        borderPane.setCenter(pane);
        scene = new Scene(borderPane, SCREENWIDTH, SCREENHEIGHT, true, SceneAntialiasing.BALANCED);
        
        System.out.println(group.getEffectiveNodeOrientation());
        //System.out.println(group.localToScene(1, 1));  
        
        //System.out.println(pane.localToScene(5, 5)); 
        //System.out.println(pane.localToParent(5, 5)); 
        
        
        primaryStage.setTitle("Breakout");
        primaryStage.setResizable(false);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setOnCloseRequest((event) -> {
            Platform.exit(); 
        });
        
        handleKeyPress();
        setDefault();
        newGame();
        
    }
    
    private void newGame(){
        System.out.println("Caricamento nuova partita");
        
        if(fileLevel.exists()){
            brickGrid.loadLevel(fileLevel);
            setDefault();
            aTimer.start();
        }
        else{
            String instruction = "Livello non trovato, premere F5 per caricare un nuovo livello" + "\n" + fileLevel.getAbsolutePath();
            Text text = new Text((scene.getWidth() - pad.getFitHeight()*15) / 2, pad.getY() - 40, instruction);
            text.setFont(Font.font("Serif", BOLD , 14));
            text.setFill(Color.RED);
        
            group.getChildren().add(text);
        }
    }
        
    private void showEditor(){
        new EditorWindow(SCREENWIDTH, SCREENHEIGHT, maxRow, maxCol);
    }
    
    private void chooseLevel(){
        pause = true;
        aTimer.stop();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Scegli Livello");
        fileChooser.setInitialDirectory(new File("Livelli/"));
        fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        File fileChosen = fileChooser.showOpenDialog(primaryStage);
        if (fileChosen != null) {
        	fileLevel = fileChosen;
        	newGame();
        }
    }
   
    private void createMenus(){
        MenuItem newGame = new MenuItem("Nuova partita                   F1");
        newGame.setOnAction( (ActionEvent e) -> {
            newGame();
        });
        
        MenuItem editorm = new MenuItem("Crea Livello Personale      F2");
        editorm.setOnAction((ActionEvent e) -> {
            showEditor();
        });
        MenuItem exit = new MenuItem("Esci                                 ESC");
        exit.setOnAction( (ActionEvent e) -> {
            closeAll();
        });
        menuFile.getItems().addAll(newGame, new SeparatorMenuItem(), editorm, new SeparatorMenuItem(), exit);
        
        MenuItem speedMod = new MenuItem("Velocità               S");
        speedMod.setOnAction( (ActionEvent e) -> {
            System.out.println("nuova finestra di dialogo con opzioni a 5 velocità");
        });
        
        MenuItem difficultyMod = new MenuItem("Scegli Livello     F5");
        difficultyMod.setOnAction( (ActionEvent e) -> {
           chooseLevel();
        });
        menuOption.getItems().addAll(speedMod, new SeparatorMenuItem(), difficultyMod);
        
        MenuItem legend = new MenuItem("Legenda                 F10");
        legend.setOnAction( (ActionEvent e) -> {
            showLegend();
        });
        menuInfo.getItems().addAll(legend);
    }
   
    
    private void runBreakOut() {
        if(!brickGrid.checkBricks(group, ball)){
            try {
                checkBorder();
                checkPad();

                ball.setX(ball.getX() + ball.getDX() * ball.getSpeed());
                ball.setY(ball.getY() + ball.getDY() * ball.getSpeed());
                //System.out.println("2°: " + ball.getDX() + ", " + ball.getDY());
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else{
            win(true);
            aTimer.stop();
        }
        
        if(!primaryStage.isFocused()){
            aTimer.stop();
            pause = true;
        }
    }
       
    
        
    private void checkBorder() {
        
        boolean atRightBorder = ball.getX() >= ( pane.getWidth()- ball.getFitWidth());
        boolean atLeftBorder = ball.getX() <= 0 ;
        boolean atTopBorder = ball.getY() <= 0;
        boolean atBottomBorder = ball.getY() >= (pane.getHeight() - ball.getFitHeight() - 3);
        
        if (atLeftBorder || atRightBorder) {
            ball.invertDX();
        }
        if ( atTopBorder) {
            ball.invertDY();
        }
        if (atBottomBorder ) {
            ball.setDX(0);
            ball.setDY(0);
            
            win(false);
            goRight=false;
            goLeft=false;
            aTimer.stop();
        }
           
    }
    
    private void checkPad(){
        boolean atPadRight = ball.intersects(pad.getX() + pad.getFitWidth(), pad.getY(), 0, pad.getFitHeight());
        boolean atPadLeft = ball.intersects(pad.getX(), pad.getY(), 0, pad.getFitHeight());
        boolean atPadTop = ball.intersects(pad.getX(), pad.getY(), pad.getFitWidth(), 0);
        //boolean atPadYCenter = ball.intersects(pad.getX(), pad.getY() + (pad.getFitHeight() + ball.getFitHeight())/2 , pad.getFitWidth() , 0);
        
        if(atPadTop && ball.getDY() > 0){
           ball.invertDY();
        }
        if(atPadRight && ball.getDX() < 0){
            ball.invertDX();
            //if(atPadYCenter){
             //   ball.invertDY();
            //}
        }
        if(atPadLeft && ball.getDX() > 0) {
        	ball.invertDX();
        }
      
       
        
        if(goLeft && (atPadTop || atPadRight || atPadLeft)){
            ball.decreaseDX();
            }
        if(goRight && (atPadTop || atPadRight || atPadLeft)){
            ball.increaseDX();
        } 
        
        
        //moving pad
        if(goLeft && pad.getX() > 0 ){
        	pad.setX(pad.getX() - pad.getDX());
            /*if(atPadTop && (atPadRight || atPadLeft)){
                pad.setX(pad.getX() - ball.getDX() * ball.getSpeed());
            }
            else{
                pad.setX(pad.getX() - pad.getDX());
            }*/
        }
        
        if(goRight && pad.getX() < scene.getWidth() - pad.getFitWidth()){
        	pad.setX(pad.getX() + pad.getDX());
        	/*if(atPadTop && (atPadRight || atPadLeft)){
                pad.setX(pad.getX() + ball.getDX() * ball.getSpeed());
            }
            else{
                pad.setX(pad.getX() + pad.getDX());
            }*/
        }
    }
    
    
    
    private void handleKeyPress() {
        scene.addEventHandler(KeyEvent.KEY_PRESSED, (KeyEvent e) -> {
            
            switch(e.getCode()){
                case LEFT:
                    goLeft = true;
                    break;
                case RIGHT:
                    goRight = true;
                    break;
                case F1:
                    newGame();
                    break;
                case F2:
                    showEditor();
                    break;
                case F5:
                    chooseLevel();
                    break;
                case F10:
                    showLegend();
                    break;
                case SPACE:
                    pause =!pause;
                    if(pause){
                       aTimer.stop();
                    }
                    else{
                        aTimer.start();
                    }
                    break;
                case S:
                    changeSpeed();
                    break;
                case ESCAPE:
                    closeAll();
                default:
                    break;
            }
        });
        scene.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent e) -> {
            
            if(e.getCode() == KeyCode.LEFT ){
                goLeft = false;
            }
            if(e.getCode() == KeyCode.RIGHT ){
                goRight = false;
            }
        });
        
    }
    
    private void showLegend(){
        pause = true;
        aTimer.stop();
        ButtonType button = new ButtonType("OK", ButtonData.OK_DONE);
        Dialog<?> dialog = new Dialog();
        dialog.getDialogPane().getButtonTypes().add(button);
        dialog.setTitle("Legenda & Crediti");
        dialog.setResizable(true);
        Text legend = new Text("Istruzioni di gioco:");
        legend.setFont(Font.font("Serif", BOLD , 16));
        Text instructions = new Text("Muoviti a destra/sinistra con le freccie direzionali\n"
                                + "F1 per iniziare una nuova partita\n"
                                + "F2 per aprire l'editor di livelli\n"
                                + "S per impostare la velocità della pallina\n"
                                + "F5 per aprire i livelli salvati (file .txt)\n"
                                + "SPAZIO per riprendere o mettere in pausa\n" 
                                + "ESC per uscire\n");
        Text creditsBold = new Text("Crediti");
        creditsBold.setFont(Font.font("Serif", BOLD , 16));
        Text credits = new Text("Stefano Giannini\n" + "@RageSpin su GitLab");
        
        VBox vbox= new VBox();
        vbox.getChildren().addAll(legend, instructions, creditsBold, credits);
        vbox.setPadding(new Insets(6));
        dialog.getDialogPane().setPrefSize(vbox.getMaxWidth(), 300);
        dialog.getDialogPane().getChildren().add(vbox);
        
        dialog.showAndWait().filter(response -> response == ButtonType.OK).ifPresent((Object response)-> {
            dialog.close();
        });
    }
    
    private void changeSpeed(){
        pause = true;
        aTimer.stop();
        ButtonType button = new ButtonType("Applica", ButtonData.OK_DONE);
        Dialog dialog = new Dialog();
        dialog.getDialogPane().getButtonTypes().add(button);
        dialog.setTitle("Modifica Velocità");
        Text speedText = new Text("Velocità pallina :");
        speedText.setFont(Font.font("Serif", BOLD , 12));
        
        Slider speedSlider = new Slider(1, 5, ball.getSpeed());
        speedSlider.setBlockIncrement(1);
        speedSlider.setMajorTickUnit(1);
        speedSlider.setMinorTickCount(0);
        speedSlider.setShowTickLabels(true);
        //speedSlider.setSnapToTicks(true);
        speedSlider.valueProperty().addListener((obs, oldval, newVal) -> 
        {
            speedSlider.setValue(Math.round(newVal.doubleValue()));
            ball.setSpeed(newVal.intValue());
        });
        speedSlider.setMinWidth(200);
        speedSlider.setMaxWidth(200);
        
        
        HBox hbox = new HBox(20);
        hbox.getChildren().addAll(speedText, speedSlider);
        hbox.setPadding(new Insets(20));
        dialog.getDialogPane().setPrefSize(hbox.getMaxWidth(), 100);
        dialog.getDialogPane().getChildren().add(hbox);
        dialog.showAndWait().filter(response -> response == ButtonType.OK).ifPresent((Object response)-> {
            dialog.close();
        });
    }
    
    private void closeAll(){
        primaryStage.close();
        Platform.exit();
    }
    
    private void win(boolean w){
        String endInstruction;
        Text text;
        if(w){
            endInstruction = "Hai Vinto, \n" +
                                "premi F5 per caricare un nuovo livello";}
        else{
            endInstruction = "Hai perso, \n" +
                                "premi F1 per giocare di nuovo";    
        }
        
        text = new Text((scene.getWidth() - pad.getFitHeight()*12) / 2, pad.getY() - 40, endInstruction);
        text.setFont(Font.font("Serif", BOLD , 14));
        text.setFill(Color.RED);
        
        group.getChildren().add(text);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
